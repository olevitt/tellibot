package core;

import utils.StringUtil;

public class Enfant {

	String nom;
	Integer age;
	String villeDepart;
	String villeArrivee;
	
	public Enfant(String nom, Integer age, String villeDepart) {
		this.nom = nom;
		this.age = age;
		this.villeDepart = villeDepart;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getVilleDepart() {
		return villeDepart;
	}

	public void setVilleDepart(String villeDepart) {
		this.villeDepart = villeDepart;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Enfant)) {
			return false;
		}
		Enfant enfant = (Enfant) obj;
		return StringUtil.equals(enfant.getNom(), getNom());
	}

	public String getVilleArrivee() {
		return villeArrivee;
	}

	public void setVilleArrivee(String villeArrivee) {
		this.villeArrivee = villeArrivee;
	}
	
	public String toString() {
		return getNom()+" "+getAge()+" ans ["+getVilleDepart()+","+getVilleArrivee()+"]";
	}
	
	
	
}
