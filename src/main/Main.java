package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import notifs.GMail;
import requetes.Initialisation;
import requetes.ListeEnfant;
import core.Enfant;

public class Main {

	static Connection conn = null;
	public static Properties props;

	public static void main(String[] args) throws Exception {
		InputStream is = new FileInputStream(new File("properties.properties"));
		props = new Properties();
		props.load(is);
		is.close();
		List<Demande> demandes = new ArrayList<Demande>();
		String userName = props.getProperty("dbuser");
		String password = props.getProperty("dbpwd");
		String url = "jdbc:mysql://localhost/telligobot";
		Class.forName ("com.mysql.jdbc.Driver").newInstance ();
		conn = DriverManager.getConnection (url, userName, password);
		System.out.println ("Database connection established");
		PreparedStatement st = conn.prepareStatement("SELECT email, idstage, nom FROM inscriptions");
		ResultSet set = st.executeQuery();
		while (set.next()) {
			String email = set.getString(1);
			String stage = set.getString(2);
			String nom = set.getString(3);
			Demande demande = new Demande(email, stage,nom);
			demandes.add(demande);
		}
		conn.close();
		System.out.println(demandes.size()+" demandes trouvées");
		if (demandes.size() == 0) {
			return;
		}
		boolean ok = new Initialisation(props.getProperty("telligouser"),props.getProperty("telligopwd")).go();
		if (!ok) {
			System.out.println("Echec de l'authentification, bisous");
			return;
		}
		System.out.println("Initialisation OK");
		for (Demande demande : demandes) {
			try {
				int stage = Integer.parseInt(demande.getStage());
			}
			catch (NumberFormatException e) {
				System.out.println("Stage invalide gros malin "+demande.getEmail()+" / "+demande.getStage());
				continue;
			}
			System.out.println("Traitement de la demande de "+demande.getEmail()+" pour la colo "+demande.getNom()+" ("+demande.getStage()+")");
			List<Enfant> enfants = new ListeEnfant(demande.getStage()).go();
			System.out.println(enfants.size()+" enfants inscrits pour la colo "+demande.getStage());
			StringBuilder builder = new StringBuilder();
			builder.append("Salut, voici les inscrits à ton séjour numéro "+demande.getStage()+System.getProperty("line.separator"));
			builder.append("Tu as "+enfants.size()+" enfants inscrits !"+System.getProperty("line.separator"));
			for (Enfant enfant : enfants) {
				builder.append(enfant.getNom()+" ("+enfant.getAge()+" ans) "+enfant.getVilleDepart()+" "+enfant.getVilleArrivee()+System.getProperty("line.separator"));
			}
			builder.append(System.getProperty("line.separator"));
			builder.append(System.getProperty("line.separator"));
			builder.append("Tellibot est une création d'Estragon : ungawa14@gmail.com");
			builder.append(System.getProperty("line.separator"));
			builder.append("Enjoy !");
			GMail.sendMail(builder.toString(), "[Tellibot] "+enfants.size()+" inscrits à la colo "+demande.getNom(), demande.getEmail());
		}



	}


}
