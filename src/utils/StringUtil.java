package utils;

public class StringUtil {

	public static boolean equals(String string1, String string2) {
		if (string1 == null || string2 == null) {
			return (string1 == string2);
		}
		return string1.equals(string2);
	}
	
	public static boolean isEmpty(String string1) {
		return string1 == null || string1.length() == 0;
	}
}
