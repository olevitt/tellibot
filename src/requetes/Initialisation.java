package requetes;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;

public class Initialisation {

	String login,mdp;
	
	public Initialisation(String login, String mdp) {
		this.login = login;
		this.mdp = mdp;
	}
	
	public boolean go() throws Exception {
		//Requete 1
		HttpPost post = new HttpPost("http://webanim2.telligo.fr/Webanim/cookie.php?do=login&url=");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("login", login));
		nameValuePairs.add(new BasicNameValuePair("pass", mdp));
		nameValuePairs.add(new BasicNameValuePair("action.x", "22"));
		nameValuePairs.add(new BasicNameValuePair("action.y", "24"));
		nameValuePairs.add(new BasicNameValuePair("action", "submit"));
		post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpClient client = ClientPool.getClient();
		HttpContext context = ClientPool.getContext();
		HttpResponse reponse = client.execute(post,context);
		Header[] location = reponse.getHeaders("location");
		post.releaseConnection();
		if (location.length != 1 || location[0].getValue().contains("errorconnexion")) {
			return false;
		}
		else {
			return true;
		}
	}
}
