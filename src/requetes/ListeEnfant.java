package requetes;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXSource;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.ccil.cowan.tagsoup.Parser;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import core.Enfant;

public class ListeEnfant {

	String idSejour;

	static String regex = "([^0-9.]*)([0-9]{1,2}) ans";
	static Pattern pattern = Pattern.compile(regex);
	List<Enfant> listeEnfants = new ArrayList<Enfant>();
	String ville = "N/A";
	static String regexVille = "(Ville de départ|Ville de retour) : (.*)";
	static Pattern patternVille = Pattern.compile(regexVille);

	public ListeEnfant(String idSejour) {
		this.idSejour = idSejour;
	}

	public List<Enfant> go() throws Exception {
		//Requete 2
		HttpGet get = new HttpGet("http://webanim2.telligo.fr/Webanim/listeEnfant.php?stg_no="+idSejour);
		HttpResponse reponse2 = ClientPool.getClient().execute(get,ClientPool.getContext());
		InputStream is2 = reponse2.getEntity().getContent(); 
		XMLReader reader = new Parser();
		reader.setFeature(Parser.namespacesFeature, false);
		reader.setFeature(Parser.namespacePrefixesFeature, false);

		Transformer transformer = TransformerFactory.newInstance().newTransformer();

		DOMResult result = new DOMResult();
		InputSource source = new InputSource(is2);
		source.setEncoding("iso-8859-1");
		transformer.transform(new SAXSource(reader, source), 
				result);

		Node node = result.getNode();
		parseNode(node);
		get.releaseConnection();
		return listeEnfants;
	}

	private void parseNode(Node node) {
		Matcher matcher;
		if (node.getNodeName().equals("#text") && !node.getNodeValue().trim().equals("")) {
			if (node.getNodeValue().startsWith("Directement sur le centre")) {
				ville = node.getNodeValue();
			}
			else if ((matcher = patternVille.matcher(node.getNodeValue())).matches()) {
				ville = matcher.group(2);
			}
			else if ((matcher = pattern.matcher(node.getNodeValue())).matches()) {
				Enfant enfant = new Enfant(matcher.group(1),Integer.parseInt(matcher.group(2)),ville);
				int index = listeEnfants.indexOf(enfant);
				if (index == -1) {
					listeEnfants.add(enfant);
				}
				else {
					listeEnfants.get(index).setVilleArrivee(ville);
				}
			}
		}
		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			parseNode(node.getChildNodes().item(i));
		}
	}
}
