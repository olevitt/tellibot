package requetes;

import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class ClientPool {

	static DefaultHttpClient client;
	static HttpContext context;
	static BasicCookieStore cookieStore;
	
	public static HttpClient getClient() {
		if (client == null) {
			init();
		}
		return client;
	}
	
	public static HttpContext getContext() {
		if (context == null) {
			init();
		}
		return context;
	}
	
	public static CookieStore getCookies() {
		if (cookieStore == null) {
			init();
		}
		return cookieStore;
	}
	
	private static void init() {
		client = new DefaultHttpClient();
		HttpProtocolParams.setUserAgent(client.getParams(), "Mozilla/5.0 (Windows; U; Windows NT 5.1; fr; rv:1.9.2.21) Gecko/20110830 Firefox/3.6.21");
		cookieStore = new BasicCookieStore();
		context = new BasicHttpContext();
		context.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
	}
}
